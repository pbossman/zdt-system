# Set path for bin, sbin 
export PATH=$PATH:/usr/bin:/usr/sbin

# ported tools environment variables
# gzip
export PATH=$PATH:/usr/rocket/bin

#bash enhanced ascii support
export _CEE_RUNOPTS="FILETAG(AUTOCVT,AUTOTAG) POSIX(ON)"
export _BPXK_AUTOCVT=ON
export _TAG_REDIR_ERR=txt
export _TAG_REDIR_IN=txt
export _TAG_REDIR_OUT=txt

#vim
export VIM=/usr/rocket/share/vim/
# Enables terminal colors
export TERMINFO=$HOME/.terminfo/x
export TERM=xterm-256color


#diffutils
export INFOPATH=$INFOPATH:/usr/rocket/info
export CHARSETALIASDIR=/usr/rocket/lib
export MANPATH=$MANPATH:/usr/rocket/share/man

#git
export PERL5LIB=/usr/rocket/share/perl/5.24.1:$PERL5LIB
# git environment variables
export GIT_SHELL=/usr/rocket/bin/bash
export GIT_EXEC_PATH=/usr/rocket/libexec/git-core
export GIT_TEMPLATE_DIR=/usr/rocket/share/git-core/templates
export GIT_MAN_PATH=/usr/rocket/man


#Perl
export PERL5LIB=$PERL5LIB:/usr/rocket/lib/perl5
export LIBPATH=$LIBPATH:/usr/rocket/lib/perl5/5.24.0/os390/CORE

# set git editor to create comments on encoding ISO8859-1
 git config --global core.editor vim

#node environment variables
#export PATH=/usr/node/node-v8.16.0-os390-s390x/bin:$PATH
export PATH=/usr/node/node-v6.17.0-os390-s390x/bin:$PATH
export COMPILER_INSTALL_HELD_MSGCLASS=H

#db2 clp
#export JAVA_HOME=/usr/lpp/java/J8.0_64
#export JAVA_HOME=/usr/lpp/java/J8.0
export CLPHOME=/usr/lpp/db2c10
export CLASSPATH=/usr/lpp/db2c10/base/lib/clp.jar
export JCCJAR=$JCCJAR:/usr/lpp/db2c10/jdbc/classes
export CLASSPATH=$CLASSPATH:$JCCJAR/db2jcc4.jar
export CLASSPATH=$CLASSPATH:$JCCJAR/db2jcc_license_cisuz.jar
export CLASSPATH=$CLASSPATH:$JCCJAR/db2jcc_license_cu.jar
export PATH=$PATH:$JAVA_HOME:$JAVA_HOME/bin
export CLPPROPERTIESFILE=~/clp.properties
alias  db2clp="$JAVA_HOME/bin/java com.ibm.db2.clp.db2"

#ZOWE 
ZOWE_EXPLORER_HOST=s0w1.dal-ebis.ihost.com
ZOWE_IPADDRESS=192.168.1.12
ZOWE_JAVA_HOME=$JAVA_HOME
ZOWE_SDSF_PATH=/usr/lpp/sdsf/java
#ZOWE_ZOSMF_PATH=/global/zosmf/configuration/servers/zosmfServer
ZOWE_ZOSMF_PATH=/var/zosmf/configuration/servers/zosmfServer
ZOWE_ZOSMF_PORT=10443

#DOE 
export DOE_INSTALL=/usr/doe
export DOE_HOME=$DOE_INSTALL/doeserver
export DOE_VAR=$DOE_HOME/var
export DOE_CONF=$DOE_HOME/conf
export DOE_LOG=$DOE_CONF/logs
export DOE_POLICY=$DOE_VAR/policy
export DOE_SVRPORT=12023
