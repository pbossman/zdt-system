if [ $(id -u) -eq 0 ];
then # you are root, make the prompt red
    PS1="\[\033[36m\]$LOGNAME\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]# "
else
    PS1="\[\033[36m\]$LOGNAME\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
fi
#PS1="\[\033[36m\]$LOGNAME\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "